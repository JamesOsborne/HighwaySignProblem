package algorithmsproject5;

/*
 * This file handles the input parsing, computation of shortest paths,
 * sign creation, and output.
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: AlgorithmsProject5.java
 * Created: Nov 2017
 * ©Copyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.lang.Integer;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author James Osborne and Nate Stahlnecker
 */
public class AlgorithmsProject5 {
    private static final HashMap<Integer, Node> highway = new HashMap<>();
    private static final ArrayList<Sign> signs = new ArrayList<>();
    private static double[][] distances;
    private static int[][] nextNode;
    private static int[] cities;
    
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length < 1) {
            System.out.println("Please provide an input filename");
        } else {
            parseInputFile(args[0]);
            
            floydWarshall();
            
            printSigns();
        }
    }
    
    private static void parseInputFile(String inputFileName) 
            throws FileNotFoundException {
        File file = new File(inputFileName);
        Scanner scan = new Scanner(file);
        
        /*
          Read in number of intersections, which is irrelevant to us,
          for we are storing in a hashmap, which is why we do not store this.
        */
        scan.nextInt();
        int roadsBetweenConnections = scan.nextInt();
        int intersectionCities = scan.nextInt();
        
        int roadStart;
        int roadEnd;
        double distance;
        
        for (int i = 0; i < roadsBetweenConnections; ++i) {
            roadStart = scan.nextInt();
            roadEnd = scan.nextInt();
            distance = scan.nextDouble();
            
            insertNode(roadStart, roadEnd, distance);
        }
        
        int intersectionNumber;
        String cityName;
        cities = new int[intersectionCities];
        
        for (int i = 0; i < intersectionCities; ++i) {
            intersectionNumber = scan.nextInt();
            cityName = scan.next();
            
            Node node = highway.get(intersectionNumber);
            node.setName(cityName);
            
            cities[i] = intersectionNumber;
        }
        
        int numberOfSigns = scan.nextInt();
        int originIntersection;
        int destinationIntersection;
        
        for (int i = 0; i < numberOfSigns; ++i) {
            originIntersection = scan.nextInt();
            destinationIntersection = scan.nextInt();
            distance = scan.nextDouble();
            
            Sign sign = new Sign(
                    originIntersection, 
                    destinationIntersection, 
                    distance); 
            
            signs.add(sign);
        }
    }
    
    private static void insertNode(int start, int end, double distance) {
        Node node = new Node();
        
        if (highway.containsKey(start)) {
            node = highway.get(start);
        }
        
        node.addNeighbor(end, distance);
        highway.put(start, node);
        
        if (highway.containsKey(end)) {
            node = highway.get(end);
        } else {
            //This creates the ending node of an edge if it does not yet exist.
            //This covers a case where a node may only be described
            //as the end of a node in the input.
            node = new Node();
        }
            
        node.addNeighbor(start, distance);
        highway.put(end, node);
    }
    
    private static void floydWarshall() {
        int numberOfVertices = highway.size();
        distances = new double[numberOfVertices][numberOfVertices];
        nextNode = new int[numberOfVertices][numberOfVertices];
        
        for (int i = 0; i < numberOfVertices; ++i) {
            Arrays.fill(distances[i], Double.POSITIVE_INFINITY);
        }
        
        Set<Integer> keys = highway.keySet();
        
        for (Integer key : keys) {
            Node node = highway.get(key);
            Set<Integer> neighbors = node.getNeighbors().keySet();
            
            for (Integer neighbor : neighbors) {
                distances[key][neighbor] = node.getNeighborDistance(neighbor);
            }
        }
        
        for (int i = 0; i < numberOfVertices; ++i) {
            for (int j = 0; j < numberOfVertices; ++j) {
                if (i != j) {
                    nextNode[i][j] = j;
                }
            }
        }
        
        for (int i = 0; i < numberOfVertices; ++i) {
            for (int j = 0; j < numberOfVertices; ++j) {
                for (int k = 0; k < numberOfVertices; ++k) {
                    if (distances[j][i] + distances[i][k] < distances[j][k]) {
                        distances[j][k] = distances[j][i] + distances[i][k];
                        nextNode[j][k] = nextNode[j][i];
                    }
                }
            }
        }
    }
    
	//printSigns determines whether signs are on shortest paths,
	//then determines distance and city name as appropriate
    private static void printSigns() {
        for (Sign sign : signs) {
            int startOfSign = sign.getStart();
            int endOfSign = sign.getEnd();
            
            for (int city : cities) {
                ArrayList<Integer> path = new ArrayList<>();
                path.add(startOfSign);
                
                if (startOfSign != city 
                        && nextNode[startOfSign][city] == endOfSign) {
                    int current = startOfSign;
                    int destination = city;

                    while (current != destination) {
                        current = nextNode[current][destination];
                        path.add(current);
                    }
                    
                    int distance = (int) Math.round(
                            distances[startOfSign][city] - sign.getDistance());
                    SignInfo info = new SignInfo(
                            getNameFromHighway(city),
                            distance,
                            path);

                    sign.addInfoToSignInfo(info);
                }
            }
            
            ArrayList<SignInfo> signInfo = sign.getSignInfo();   
            sortSignInfo(signInfo);
            
            for (SignInfo info : signInfo) {
                System.out.printf(
                        "%-20s %d\n",
                        info.getCityName(), 
                        info.getDistance());
            }
            
            System.out.println();
        }
    }
    
    private static String getNameFromHighway(int intersection) {
        if (highway.containsKey(intersection)) {
            return highway.get(intersection).getName();
        }
        
        return null;
    }
    
    private static void sortSignInfo(ArrayList<SignInfo> signInfo) {
        if (signInfo != null) {
            Collections.sort(signInfo, SignInfo.getComparator());
        }
    }
}
