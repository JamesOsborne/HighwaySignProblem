package algorithmsproject5;

/*
 * Sign class provides a template for Sign objects
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: Sign.java
 * Created: Nov 2017
 * ©Copyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

import java.util.ArrayList;

/**
 *
 * @author James Osborne and Nate Stahlnecker
 */
public class Sign {
    private int start;
    private int end;
    private double distance;
    private ArrayList<SignInfo> signInfo;
    
    public Sign() {
        this(-1, -1, -1.0);
    }
    
    public Sign(int s, int e, Double d) {
        this.start = s;
        this.end = e;
        this.distance = d;
        this.signInfo = new ArrayList<>();
    }
    
    public int getStart() {
        return this.start;
    }
    
    public int getEnd() {
        return this.end;
    }
    
    public double getDistance() {
        return this.distance;
    }
    
    public ArrayList<SignInfo> getSignInfo() {
        return this.signInfo;
    }
    
    public void setStart(int s) {
        this.start = s;
    }
    
    public void setEnd(int e) {
        this.end = e;
    }
    
    public void setDistance(double dist) {
        this.distance = dist;
    }
    
    public void addInfoToSignInfo(SignInfo info) {
        if (this.signInfo == null) {
            this.signInfo = new ArrayList<>();
        }
        
        this.signInfo.add(info);
    }
}
