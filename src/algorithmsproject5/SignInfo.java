package algorithmsproject5;

/*
 * SignInfo class provides a template for SignInfo objects
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: SignInfo.java
 * Created: Nov 2017
 * ©Copyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author James Osborne and Nate Stahlnecker
 */
public class SignInfo {
    private String cityName;
    private int distance;
    private ArrayList<Integer> path;
    
    public SignInfo(String name, int dist, ArrayList<Integer> p) {
        this.cityName = name;
        this.distance = dist;
        this.path = p;
    }
    
    public String getCityName() {
        return this.cityName;
    }
    
    public int getDistance() {
        return this.distance;
    }
    
    public ArrayList<Integer> getPath() {
        return this.path;
    }
    
    public void setCityName(String name) {
        this.cityName = name;
    }
    
    public void setDistance(int dist) {
        this.distance = dist;
    }
    
    public void setPath(ArrayList<Integer> p) {
        this.path = p;
    }
    
    public static SignInfoComparator getComparator() {
        return new SignInfoComparator();
    }
    
    public static class SignInfoComparator implements Comparator<SignInfo> {
        @Override
        public int compare(SignInfo signInfo1, SignInfo signInfo2) {
            return Integer.compare(signInfo1.distance, signInfo2.distance);
        }
    }
}