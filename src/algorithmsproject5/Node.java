package algorithmsproject5;

/*
 * Node class provides a template for Node objects
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: Node.java
 * Created: Nov 2017
 * ©Copyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

import java.util.HashMap;

/**
 *
 * @author James Osborne and Nate Stahlnecker
 */
public class Node {
    private HashMap<Integer, Double> neighbors;
    private String name;
    
    public Node() {
        this(new HashMap<>(), "");
    }
    
    public Node(HashMap<Integer, Double> nMap, String n) {
        this.neighbors = nMap;
        this.name = n;
    }
    
    public HashMap<Integer, Double> getNeighbors() {
        return this.neighbors;
    }
    
    public double getNeighborDistance(int neighbor) {
        if (this.neighbors != null && this.neighbors.containsKey(neighbor)) {
            return this.neighbors.get(neighbor);
        }
        
        return -1;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setNeighbors(HashMap<Integer, Double> nMap) {
        this.neighbors = nMap;
    }
    
    public void setName(String n) {
        this.name = n;
    }
    
    public void addNeighbor(int location, double dist) {
        if (this.neighbors == null) {
            this.neighbors = new HashMap<>();
        }
        
        this.neighbors.put(location, dist);
    }
}
